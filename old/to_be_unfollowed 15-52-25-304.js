'use strict';

var users = {};

users.checkToUnfollow = function(db, callback) {
    var followers = db.collection("our_followers");
    var followed = db.collection("followed");
    var getfollowers = db.collection("get_followers");
    var tounfollow = db.collection("tounfollow");

    var date = new Date();
    date.setDate(date.getDate() - 3);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    followed.find({
        created: date
    }).toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        var total = data.length;

        function update() {
            var id = data[total - 1].id;
            var followedData = data[total - 1];
            followers.findOne({
                "user.id": id
            }, function(err, doc) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                if (doc) {

                    tounfollow.insert(followedData, function(err, r) {
                        if (err) {
                            console.log(err);
                            //throw err;
                        }
                        console.log(r.result);

                        getfollowers.insert(followedData, function(err, r) {
                            if (err) {
                                console.log(err);
                            }
                            console.log(r);
                            followed.removeOne(followedData, function(err, r) {
                                if (err) {
                                    console.log(err);
                                }
                                console.log(r);
                                if (--total) {
                                    update();
                                } else {
                                    console.log("ALL DONE");
                                    if (typeof callback === "function") {
                                        callback(null, {
                                            done: true
                                        });
                                    }
                                }
                            });
                        });
                    });

                } else {
                    if (--total) {
                        update();
                    } else {
                        console.log("ALL DONE");
                        if (typeof callback === "function") {
                            callback(null, {
                                done: true
                            });
                        }
                    }
                }
            });
        }
        if (total) {
            update();
        }
    });
};

module.exports = users;
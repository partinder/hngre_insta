'use strict';

var instagram = require("./instagram");
var request = require("request");
var config = require("./commons/config");

//52.5.7.175

var accessToken = config.accessToken;
var url = "mongodb://" + config.host + ":27017/db_hngreV2";
var userId = "";

var users = {};

users.storeOurFollowings = function(db) {

    function getFollowers(cursor) {
        var api = "https://api.instagram.com/v1/users/" + userId + "/follows?access_token=" + accessToken + "&cursor=" + cursor;
        request.get(api, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                body = JSON.parse(body);
                if (body.data) {
                    console.log(body.pagination);
                    var collection = db.collection("our_followings");
                    collection.insert(body.data, function(err, r) {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                        console.log(r.result)
                        if (body.pagination && body.pagination.next_cursor) {
                            setTimeout(function() {
                                getFollowers(body.pagination.next_cursor);
                            }, 1000);
                        }
                    });

                }

            }
        });
    }
};

//users.storeOurFollowings(db);

module.exports = users;
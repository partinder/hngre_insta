'use strict';

var MongoClient = require("mongodb").MongoClient;
//52.5.7.175
var url = "mongodb://52.5.7.175:27017/db_hngreV2";
var MIN = 1;
var MAX = 1001;

var aggregate = {};

aggregate.findUserWhoLikes = function(min, max, callback) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                likes: 1
            }
        }, {
            $unwind: "$likes.data"
        }, {
            $group: {
                _id: "$likes.data.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }], {
            allowDiskUse: true,
            cursor: {}
        }).toArray(function(err, result) {
            if (err) {
                console.log(err);
                callback(err, []);
            } else {
                console.log(result.length);
                callback(null, result);
            }

            db.close();
        });
    });
};

aggregate.findUsersWhoCommented = function(min, max, callback) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                comments: 1
            }
        }, {
            $unwind: "$comments.data"
        }, {
            $group: {
                _id: "$comments.data.from.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }], {
            allowDiskUse: true,
            cursor: {}
        }).toArray(function(err, result) {
            if (err) {
                console.log(err);
                callback(err, []);
            } else {
                console.log(result.length);
                callback(null, result);
            }
            db.close();
        });

    });
};

aggregate.findUsersWhoPostedOnLocations = function(min, max, callback) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                user: 1
            }
        }, {
            $group: {
                "_id": "$user.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }]).toArray(function(err, result) {
            if (err) {
                console.log(err);
                callback(err, []);
            } else {
                console.log(result.length);
                callback(null, result);
            }
            db.close();
        });
    });
};

aggregate.insertUserIterator = function(user, callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("user_iterator");
        collection.insert(user, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

aggregate.getMerchantUsers = function(callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("users");
        collection.find().toArray(function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

aggregate.startFromMerchants = function() {
    aggregate.findUsersWhoPostedOnLocations(MIN, MAX, function(err, result) {
        console.log(result);

        var total = result.length;

        function insertUserIterator() {

            aggregate.insertUserIterator({
                id: result[total - 1]._id
            }, function(err, res) {
                if (err) {
                    console.log(err);
                    throw err;
                } else {
                    console.log(res);
                }
                if (--total) {
                    insertUserIterator();
                } else {
                    console.log("end");
                }
            });
        }

        if (total) {
            insertUserIterator();
        }

    });
};

aggregate.startFromUsers = function() {
    aggregate.getMerchantUsers(function(err, result) {
        console.log(result);

        var total = result.length;

        function insertUserIterator() {

            aggregate.insertUserIterator({
                id: result[total - 1]._id
            }, function(err, res) {
                if (err) {
                    console.log(err);
                    throw err;
                } else {
                    console.log(res);
                }
                if (--total) {
                    insertUserIterator();
                } else {
                    console.log("end");
                }
            });
        }

        if (total) {
            insertUserIterator();
        }

    });
};

//aggregate.startFromMerchants();

module.exports = aggregate;



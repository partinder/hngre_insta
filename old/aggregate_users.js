'use strict';

var instagram = require("./instagram");
var request = require("request");
var config = require("./commons/config");
var MongoClient = require("mongodb").MongoClient;
//52.5.7.175

var accessToken = config.accessToken;
var url = "mongodb://" + config.host + ":27017/db_hngreV2";

var users = {};

users.getUserIterator = function(callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("user_iterator");
        collection.find().toArray(function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

users.insertUser = function(user, callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("users");
        collection.insert(user, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

users.removeUserIterator = function(user, callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("user_iterator");
        collection.removeOne(user, {
            w: 1
        }, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });

    });
};

users.storeUsers = function() {
    users.getUserIterator(function(err, data) {
        var total2 = data.length;
        function getUser() {
            //console.log(data[total2 - 1].id);
            var api = "https://api.instagram.com/v1/users/" + data[total2 - 1].id + "?access_token=" + accessToken;
            //console.log(api);
            request.get(api, function(err, res, body) {
                //console.log(body);
                if (!err && res.statusCode == 200) {
                    body = JSON.parse(body);
                    users.insertUser(body.data, function(err, res) {
                        if (err) {
                            console.log(err);
                            throw err;
                        } else {
                            console.log(res);
                            users.removeUserIterator({
                                id: data[total2 - 1].id
                            }, function(err, r) {
                                if (err) {
                                    console.log(err);
                                    throw err;
                                } else setTimeout(function() {
                                    if (--total2) {
                                        getUser();
                                    } else {
                                        console.log("end");
                                    }
                                }, 1000);
                            });

                        }
                    });

                } else {
                    if (--total2) {
                        setTimeout(function() {
                            getUser();
                        }, 1000);
                    } else {
                        console.log("end");
                    }
                }
            });
        }
        
        if (err) {
            console.log(err);
            throw err;
        } else {
            if (total2)
                getUser();
            else
                console.log("no users");
        }
    });
};

//users.storeUsers();

module.exports = users;

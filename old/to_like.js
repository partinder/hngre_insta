'use strict';

var like = {};

like.like = function(db, callback) {
    var posts = db.collection("posts");
    var to_like = db.collection("to_like");

    posts.find({
        "liked": {
            "$ne": true
        }
    }, {
        limit: 900
    }, function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        to_like.insert(data, function(err, res) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log(res.result);
        });
    });
};

module.exports = like;
'use strict';

//var followers = require("./our_followers");
var users = {};

users.checkFollowers = function(db, callback) {
    var followers = db.collection("our_followers");
    var users = db.collection("users");
    var getfollowers = db.collection("get_followers");

    followers.find().toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        var total = data.length;

        function update() {
            var id = data[total - 1].id;
            users.findOne({
                "user.id": id
            }, function(err, doc) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                if (doc) {

                    getfollowers.insert(doc, function(err, r) {
                        if (err) {
                            console.log(err);
                            //throw err;
                        }
                        console.log(r.result);

                        users.removeOne(doc, function(err, r) {
                            if (err) {
                                console.log(err);
                            }
                            if (--total) {
                                update();
                            } else {
                                console.log("ALL DONE");
                                if (typeof callback === "function") {
                                    callback(null, {
                                        done: true
                                    });
                                }
                            }

                        });
                    });

                } else {
                    if (--total) {
                        update();
                    } else {
                        console.log("ALL DONE");
                        if (typeof callback === "function") {
                            callback(null, {
                                done: true
                            });
                        }
                    }
                }
            });
        }
        if (total) {
            update();
        }
    });
};

users.getToFollow = function(db, callback) {
    var users = db.collection("users");
    users.find({}, {}, {
        limit: 240
    }, function(err, data) {
        if (err) {
            console.log(err);
            if (typeof callback === "function") {
                callback(err, null);
            }
        } else {
            if (typeof callback === "function") {
                callback(null, data);
            } else {
                console.log(data.length);
            }
        }
    });
};

users.storeToFollow = function(tofollow, db, callback) {
    var to_follow = db.collection("to_follow");
    to_follow.insert(tofollow, function(err, r) {
        if (err) {
            console.log(err);
        }
        if (typeof callback === "function") {
            console.log(r.result);
            callback(null, r);
        }
    });
};


module.exports = users;
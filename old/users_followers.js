'use strict';

var instagram = require("./instagram");
var request = require("request");
var config = require("./commons/config");
var MongoClient = require("mongodb").MongoClient;
var no_of_followers = 1000;
var factor = no_of_followers / 50;
//52.5.7.175

var accessToken = config.accessToken;
var url = "mongodb://" + config.host + ":27017/db_hngreV2";

var users = {};

users.getUserIterator = function(callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("user_iterator");
        collection.find().toArray(function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

users.insertUser = function(user, callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("users");
        collection.insert(user, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });
    });
};

users.removeUserIterator = function(user, callback) {
    MongoClient.connect(url, function(err, db) {
        var collection = db.collection("user_iterator");
        collection.removeOne(user, {
            w: 1
        }, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
            db.close();
        });

    });
};

users.storeUsers = function() {
    users.getUserIterator(function(err, data) {

        var total1 = data.length;
        var count = 0;

        function getFollowers(cursor) {
            request.get("https://api.instagram.com/v1/users/" + data[total1 - 1]._id + "/followed-by?access_token=" + accessToken + "&cursor=" + cursor, function(err, res, body) {
                if (!err && res.statusCode == 200) {
                    console.log(total1);
                    body = JSON.parse(body);
                    if (body.data) {
                        users.insertUser(body.data, function(err, res) {
                            if (err) {
                                console.log(err);
                                if (--total1) {
                                    getFollowers("");
                                } else {
                                    count = 0;
                                }
                            } else {

                                users.removeUserIterator({
                                    id: data[total1 - 1].id
                                }, function(err, r) {
                                    if (err) {
                                        console.log(err);
                                        throw err;
                                    } else {
                                        console.log(body.pagination);
                                        if (body.pagination && body.pagination.next_cursor && ++count <= factor) {
                                            setTimeout(function() {
                                                getFollowers(body.pagination.next_cursor);
                                            }, 1000);
                                        } else {
                                            if (count > factor) {
                                                count = 0;
                                            }
                                            if (--total1) {
                                                getFollowers("");
                                            } else {
                                                count = 0;
                                            }
                                        }
                                    }
                                });

                            }
                        });
                    } else {
                        if (--total1) {
                            getFollowers("");
                        } else {
                            count = 0;
                        }
                    }

                } else {
                    if (--total1) {
                        getFollowers("");
                    } else {
                        count = 0;
                    }
                }
            });
        }

        if (err) {
            console.log(err);
            throw err;
        } else {
            if (total1)
                getFollowers("");
            else
                console.log("no users");

        }

    });
};

//users.storeUsers();

module.exports = users;
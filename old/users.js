'use strict';

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://52.5.7.175:27017/db_hngreV2";

//52.5.7.175

var findUserWhoLikes = function(min, max) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                likes: 1
            }
        }, {
            $unwind: "$likes.data"
        }, {
            $group: {
                _id: "$likes.data.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }], {
            allowDiskUse: true,
            cursor: {}
        }).toArray(function(err, result) {
            if (err) {
                console.log(err);

            } else {
                console.log(result.length);
            }

            db.close();
        });
    });
};

var findUsersWhoCommented = function(min, max) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                comments: 1
            }
        }, {
            $unwind: "$comments.data"
        }, {
            $group: {
                _id: "$comments.data.from.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }], {
            allowDiskUse: true,
            cursor: {}
        }).toArray(function(err, result) {
            if (err) {
                console.log(err);

            } else {
                console.log(result.length);
            }
            db.close();
        });

    });
};

var findUsersWhoPostedOnLocations = function(min, max) {
    MongoClient.connect(url, function(err, db) {

        var MerchantPics = db.collection("merchantPics");

        MerchantPics.aggregate([{
            $project: {
                merchantId: 1,
                user: 1
            }
        }, {
            $group: {
                "_id": "$user.id",
                merchants: {
                    $addToSet: "$merchantId"
                }
            }
        }, {
            $unwind: "$merchants"
        }, {
            $group: {
                _id: "$_id",
                count: {
                    $sum: 1
                },
                mcts: {
                    $push: "$merchants"
                }
            }
        }, {
            $match: {
                count: {
                    $gte: min,
                    $lt: max
                }
            }
        }]).toArray(function(err, result) {
            if (err) {
                console.log(err);

            } else {
                console.log(result.length);

            }
            db.close();
        });
    });
};

//findUsersWhoPostedOnLocations(100,1000); // 12
//findUsersWhoPostedOnLocations(50,100); // 118
//findUsersWhoPostedOnLocations(40,50); // 147
//findUsersWhoPostedOnLocations(30,40); // 370
//findUsersWhoPostedOnLocations(20,30); // 1209
//findUsersWhoPostedOnLocations(10,20); // 7783
//findUsersWhoPostedOnLocations(5,10); // 27678
//findUsersWhoPostedOnLocations(1,4); // 380427
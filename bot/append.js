/**
 * Created by Partinder on 9/2/15.
 */
var spawn = require('child_process').spawn;
var config = require("../commons/config")
var fs = require("fs")
var async = require("async")

var d = new Date().toString()




// Create the Chrome processing order.

module.exports = function(toRun, toLike1, toLike2, db, callback) {


    fs.appendFileSync("./bot/" + toRun + ".js", "//" + d + "\n")
    fs.appendFileSync("./bot/" + toRun + ".js", fs.readFileSync("./bot/web.txt", "utf8").toString())


    fs.appendFileSync("./bot/" + toLike1 + ".js", "//" + d + "\n");
    fs.appendFileSync("./bot/" + toLike1 + ".js", fs.readFileSync("./bot/webLikes.txt", "utf8").toString());


    fs.appendFileSync("./bot/" + toLike2 + ".js", "//" + d + "\n");
    fs.appendFileSync("./bot/" + toLike2 + ".js", fs.readFileSync("./bot/webLikes.txt", "utf8").toString());


    var toFollow = db.collection("to_follow")
    var toLike = db.collection("to_like")
    var toUnfollow = db.collection("to_unfollow")

    async.parallel({
        toFollow: function(callback) {
            toFollow.find({}, {
                limit: config.max_follow_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        },
        toLike: function(callback) {
            toLike.find({}, {
                limit: config.max_like_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        },
        toUnfollow: function(callback) {
            toUnfollow.find({}, {
                limit: config.max_unfollow_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        }
    }, function(err, results) {
        if (err) throw err;
        else {
            var toRun1 = results.toFollow.length / 4;
            var toHalfRun = toRun1 / 2;

            var toFollowCount = results.toFollow.length
            var toUnfollowCount = results.toUnfollow.length
            var toLikeCount = results.toLike.length
            var counter = 0;

            console.log("toFollowCount %s, toUnfollowCount %s, toLikeCount %s", toFollowCount, toUnfollowCount, toLikeCount);
            for (var iteration = 0; iteration < toRun1; iteration++) {
                var appendList = []
                var toFollowThis = results.toFollow.splice(0, 4)
                var toUnfollowThis = results.toUnfollow.splice(0, 3)
                var toLikeThis = results.toLike.splice(0, 15)

                //console.log(toFollowThis,toUnfollowThis,toLikeThis);
                if (counter++ < toHalfRun) {
                    for (var i in toFollowThis) {
                        appendList.push({
                            type: "Follow",
                            id: toFollowThis[i].user.id,
                            username: toFollowThis[i].user.username,
                        })
                    }
                    for (var i in toUnfollowThis) {
                        appendList.push({
                            type: "UnFollow",
                            id: toUnfollowThis[i].user.id,
                            username: toUnfollowThis[i].user.username
                        })
                    }
                    for (var i in toLikeThis) {
                        appendList.push({
                            type: "Like1",
                            id: toLikeThis[i].id
                        })
                    }
                    appendList = shuffle(appendList)

                    appendList.forEach(function(appFunc) {
                        if (appFunc.type == "Follow") {
                            toFollowAppend(appFunc.username, appFunc.id);
                        }
                        if (appFunc.type == "UnFollow") {
                            toUnfollowAppend(appFunc.username, appFunc.id);
                        }
                        if (appFunc.type == "Like1") {
                            toLike1Append(appFunc.id);

                        }
                    })
                    fs.appendFileSync("./bot/" + toRun + ".js", ".pause("+config.ends+")\n");
                    fs.appendFileSync("./bot/" + toLike1 + ".js", ".pause("+config.ends+")\n");
                    console.log("First Half Done", iteration)


                } else {
                    for (var i in toFollowThis) {
                        appendList.push({
                            type: "Follow",
                            id: toFollowThis[i].user.id,
                            username: toFollowThis[i].user.username
                        })
                    }
                    for (var i in toUnfollowThis) {
                        appendList.push({
                            type: "UnFollow",
                            id: toUnfollowThis[i].user.id,
                            username: toUnfollowThis[i].user.username
                        })
                    }
                    for (var i in toLikeThis) {
                        appendList.push({
                            type: "Like2",
                            id: toLikeThis[i].id
                        })
                    }
                    appendList = shuffle(appendList)
                    appendList.forEach(function(appFunc) {
                        if (appFunc.type == "Follow") {
                            toFollowAppend(appFunc.username, appFunc.id)
                        }
                        if (appFunc.type === "UnFollow") {
                            toUnfollowAppend(appFunc.username, appFunc.id)
                        }
                        if (appFunc.type == "Like2") {
                            toLike2Append(appFunc.id)
                        }
                    })
                    fs.appendFileSync("./bot/" + toRun + ".js", ".pause("+config.ends+")\n")
                    fs.appendFileSync("./bot/" + toLike2 + ".js", ".pause("+config.ends+")\n")
                    console.log("Second half Done", iteration);
                }

            }



            fs.appendFileSync("./bot/" + toRun + ".js", ".then(function(){callback(this)})\n}");
            fs.appendFileSync("./bot/" + toLike1 + ".js", ".then(function(){callback(this)})\n}");
            fs.appendFileSync("./bot/" + toLike2 + ".js", ".then(function(){callback(this)})}");
            console.log("All Done");
            callback();
        }
    })

    function toFollowAppend(username, id) {

        var url = "https://www.instagram.com/" + username
        var data = '\n.url("' + url + '").' +
            'pause('+config.pauses+')' +
            '.isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button")' +
            '.then(function(isThere){' +
            '    if(isThere)' +
            '   {' +
            '        this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button");' +
            '        followers.insertRemoveFollow(db,"' + id + '","' + username + '",function(err){' +
            '           if(err)' +
            '           {  ' +
            '               console.log(err); ' +
            '               throw err; ' +
            '           }' +
            '           else' +
            '           {' +
            '               console.log("Followed: ' + username + '"); ' +
            '           }' +
            '       }); ' +
            '   }' +
            '   else' +
            '   { ' +
            '       this.isExisting("div.root > div.page > div.main > div.error-container")' +
            '       .then(function(isThere){' +
            '        if(isThere)' +
            '        {' +
            '            console.log("' + username + '"," Not Found");' +
            '            this.saveScreenshot("./screenshots/' + username + '.png")' +
            '           .then(function(){console.log("' + username + '");});' +
            '            followers.removeUser(db,"' + id + '",function(err){' +
            '                if(!err) console.log(" so user removed"); ' +
            '            }); ' +
            '        }' +
            '        else' +
            '        { ' +
            '            this.isExisting("a[]"' +
            '            console.log("' + username + '","Element Not Found");' +
            '            this.saveScreenshot("./screenshots/' + username + '.png")' +
            '            .then(function(){console.log("' + username + '.png");' +
            '               followers.sendMail("'+username+'", "user"); ' +
            '               this.endAll()' +
            '              .then(function(){ throw { error: "some went wrong!!" }; });' +
            '            }); ' +
            '         } ' +
            '         }); ' +
            '     } ' +
            '}).pause('+config.pauses+').back()\n';

        fs.appendFileSync("./bot/" + toRun + ".js", data)
    }

    function toUnfollowAppend(username, id) {

        var url = "https://www.instagram.com/" + username
        var data = '\n.url("' + url + '").pause('+config.pauses+').isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button").then(function(isThere){  if(isThere){ this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button").pause(7000); followers.insertRemoveUnfollow(db,"' + id + '",function(err){if(err) { console.log(err); throw err; }else{console.log("UnFollowed: ' + username + '")} }) ;}else{ this.isExisting("div.root > div.page > div.main > div.error-container").then(function(isThere){ if(isThere) { console.log("' + username + '"," Not Found"); this.saveScreenshot("./screenshots/' + username + '.png").then(function(){console.log("' + username + '")}); followers.removeUser(db,"' + id + '",function(err){ if(!err) console.log(" so user removed"); }); }else{ console.log("' + username + '","Element Not Found"); this.saveScreenshot("./screenshots/' + username + '.png").then(function(){console.log("' + username + '.png");followers.sendMail("'+username+'","user");  this.endAll().then(function(){ throw { error: "some went wrong!!" }; });  }); } }); }}).pause('+config.pauses+').back()\n';

        fs.appendFileSync("./bot/" + toRun + ".js", data)
    }

    function toLike1Append(id) {

        var url = id
        var spitted = url.split("/")[4];
        var data = '\n.url("' + url + '").pause('+config.pauses+').isExisting("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a").then(function(isThere){    if(isThere){this.click("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a"); followers.updateRemoveLike(db,"' + id + '",function(err){if(err) { console.log(err); throw err; }else{console.log("Liked: ' + id + '")} }) ; }else{ this.isExisting("div.root > div.page > div.main > div.error-container").then(function(isThere){ if(isThere) { console.log("' + id + '"," Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + url + '")}); followers.removeLike(db,"' + id + '", function(err){ if(!err)console.log("so link removed "); }); }else{ console.log("' + spitted + '","Element Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + spitted + '.png");followers.sendMail("'+spitted+'", "post");  this.endAll().then(function(){ throw { error: "some went wrong!!" }; });;  }); } }) ; }}).pause('+config.pauses+')\n';

        fs.appendFileSync("./bot/" + toLike1 + ".js", data)
    }

    function toLike2Append(id) {

        var toAppend = []
        var url = id;
        var spitted = url.split("/")[4];

        var data = '\n.url("' + url + '").pause('+config.pauses+').isExisting("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a").then(function(isThere){    if(isThere){this.click("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a"); followers.updateRemoveLike(db,"' + id + '",function(err){if(err){  console.log(err); throw err; }else{console.log("Liked: ' + id + '")} }) ; }else{ this.isExisting("div.root > div.page > div.main > div.error-container").then(function(isThere){ if(isThere) { console.log("' + id + '"," Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + url + '")}); followers.removeLike(db,"' + id + '", function(err){ if(!err)console.log("so link removed "); }); }else{ console.log("' + spitted + '","Element Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + spitted + '.png");followers.sendMail("'+spitted+'", "post");  this.endAll().then(function(){ throw { error: "some went wrong!!" }; });;  }); } }) ; }}).pause('+config.pauses+')\n';

        fs.appendFileSync("./bot/" + toLike2 + ".js", data);
    }



    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}
/**
 * Created by Partinder on 9/23/15.
 */
/**
 * Created by Partinder on 9/2/15.
 */
var spawn = require('child_process').spawn;
var config = require("../commons/config")
var fs = require("fs")
var async = require("async")

var d = new Date().toString()




// Create the Chrome processing order.

module.exports = function(toRun, db, callback) {


    fs.appendFileSync("./bot/" + toRun + ".js", "//" + d + "\n")
    fs.appendFileSync("./bot/" + toRun + ".js", fs.readFileSync("./bot/web.txt", "utf8").toString())

    var toFollow = db.collection("to_follow")
    var toLike = db.collection("to_like")
    var toUnfollow = db.collection("to_unfollow")

    async.parallel({
        toFollow: function(callback) {
            toFollow.find({}, {
                limit: config.max_follow_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        },
        toLike: function(callback) {
            toLike.find({}, {
                limit: config.max_like_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        },
        toUnfollow: function(callback) {
            toUnfollow.find({}, {
                limit: config.max_unfollow_cycle
            }).toArray(function(err, result) {
                if (err) callback(err, null)
                else {
                    callback(null, result)
                }
            })
        }
    }, function(err, results) {
        if (err) throw err;
        else {
            var toRun1 = Math.ceil(results.toFollow.length / 4)
            var toFollowCount = results.toFollow.length
            var toUnfollowCount = results.toUnfollow.length
            var toLikeCount = results.toLike.length
            var totalActions = 0

            console.log("toFollowCount %s, toUnfollowCount %s, toLikeCount %s", toFollowCount, toUnfollowCount, toLikeCount);
            for (var iteration = 0; iteration < toRun1; iteration++) {
                var appendList = []
                var toFollowThis = results.toFollow.splice(0, 4)
                var toUnfollowThis = results.toUnfollow.splice(0, 4)
                var toLikeThis = results.toLike.splice(0, 13)

                for (var i in toFollowThis) {
                    totalActions++
                    appendList.push({
                        type: "Follow",
                        id: toFollowThis[i].user.id,
                        username: toFollowThis[i].user.username
                    })
                    //console.log("Follow : "toFollowThis[i].user.username)
                }
                for (var i in toUnfollowThis) {
                    totalActions++
                    appendList.push({
                        type: "UnFollow",
                        id: toUnfollowThis[i].user.id,
                        username: toUnfollowThis[i].user.username
                    })
                }
                for (var i in toLikeThis) {
                    totalActions++
                    appendList.push({
                        type: "Like",
                        id: toLikeThis[i].id
                    })
                    //console.log(toLikeThis[i].id)
                }
                //appendList = shuffle(appendList)

                appendList.forEach(function(appFunc) {
                    if (appFunc.type == "Follow") {
                        toFollowAppend(appFunc.username, appFunc.id);
                    }
                    if (appFunc.type == "UnFollow") {
                        toUnfollowAppend(appFunc.username, appFunc.id);
                    }
                    if (appFunc.type == "Like") {
                        toLikeAppend(appFunc.id);
                    }
                })
                fs.appendFileSync("./bot/" + toRun + ".js", ".pause(" + config.ends + ")\n");

            }
            fs.appendFileSync("./bot/" + toRun + ".js", ".then(function(){callback(this)})\n}");
            console.log("All Done, Total Actions this time : %s", totalActions);
            callback();
        }
    })

    function toFollowAppend(username, id) {

        var url = "https://www.instagram.com/" + username
        var data =
            '    .url("' + url + '")\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 1000, config.pauses) + ')\n' +
            '    .isExisting("a[data-reactid=\'.0.2.0.1.$loginLink\']")\n' +
            '    .then(function (login) {\n' +
            '        if(login)\n' +
            '        { \n' +
            '            this.url("https://www.instagram.com/accounts/login")\n' +
            '                .getHTML("form[data-reactid=\'.0.0.1.0.1\']", function (err, res) {\n' +
            '                })\n' +
            '                .setValue("#lfFieldInputUsername", "' + config.instagram.username + '")\n' +
            '                .setValue("#lfFieldInputPassword", "' + config.instagram.password + '")\n' +
            '                .click("button[data-reactid=\'.0.0.1.0.1.3\']")\n' +
            '                .pause(2000)\n' +
            '                .getCookie()\n' +
            '                .then(function(cookies_){\n' +
            '                    console.log(cookies_.length);\n' +
            '                    cookies_.forEach(function(cookie){\n' +
            '                       Cookie.setter(cookie.name, cookie);\n' +
            '                    });\n' +
            '                   console.log("Saved New Cookies to File for next time Use")\n' +
            '                   Cookie.save()\n' +
            '                })\n' +
            '                .pause(500)\n' +
            '                .url("' + url + '")\n' +
            '                .isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button")\n' +
            '                .then(function(button){\n' +
            '                    if(button)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button");\n' +
            '                        followers.insertRemoveFollow(db,"' + id + '","' + username + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("Followed: ' + username + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + username + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '");});\n' +
            '                                    followers.removeUser(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so user removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + username + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '.png");\n' +
            '                                            followers.sendMail("' + username + '", "user");\n' +
            '                                            this.endAll()\n' +
            '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n' +
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '        else\n' +
            '        {\n' +
            '            this.isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button")\n' +
            '                .then(function(button){\n' +
            '                    if(button)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button");\n' +
            '                        followers.insertRemoveFollow(db,"' + id + '","' + username + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("Followed: ' + username + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + username + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '");});\n' +
            '                                    followers.removeUser(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so user removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + username + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '.png");\n' +
            '                                            followers.sendMail("' + username + '", "user");\n' +
            '                                            this.endAll()\n' +
            '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n' +
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '    })\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n\n\n'


        fs.appendFileSync("./bot/" + toRun + ".js", data)
    }

    function toUnfollowAppend(username, id) {

        var url = "https://www.instagram.com/" + username
        var data =
            '    .url("' + url + '")\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 1000, config.pauses) + ')\n' +
            '    .isExisting("a[data-reactid=\'.0.2.0.1.$loginLink\']")\n' +
            '    .then(function (login) {\n' +
            '        if(login)\n' +
            '        { \n' +
            '            this.url("https://www.instagram.com/accounts/login")\n' +
            '                .getHTML("form[data-reactid=\'.0.0.1.0.1\']", function (err, res) {\n' +
            '                })\n' +
            '                .setValue("#lfFieldInputUsername", "' + config.instagram.username + '")\n' +
            '                .setValue("#lfFieldInputPassword", "' + config.instagram.password + '")\n' +
            '                .click("button[data-reactid=\'.0.0.1.0.1.3\']")\n' +
            '                .pause(2000)\n' +
            '                .getCookie()\n' +
            '                .then(function(cookies_){\n' +
            '                    console.log(cookies_.length);\n' +
            '                    cookies_.forEach(function(cookie){\n' +
            '                       Cookie.setter(cookie.name, cookie);\n' +
            '                    });\n' +
            '                   console.log("Saved New Cookies to File for next time Use")\n' +
            '                   Cookie.save()\n' +
            '                })\n' +
            '                .pause(500)\n' +
            '                .url("' + url + '")\n' +
            '                .isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button")\n' +
            '                .then(function(button){\n' +
            '                    if(button)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button");\n' +
            '                        followers.insertRemoveUnfollow(db,"' + id + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("UnFollowed: ' + username + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + username + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '");});\n' +
            '                                    followers.removeUser(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so user removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + username + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '.png");\n' +
            '                                            followers.sendMail("' + username + '", "user");\n' +
            '                                            this.endAll()\n' +
            '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n' +
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '        else\n' +
            '        {\n' +
            '            this.isExisting("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button")\n' +
            '                .then(function(button){\n' +
            '                    if(button)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > article > header > div.-cx-PRIVATE-ProfilePage__authorInfo > div.-cx-PRIVATE-ProfilePage__usernameAndFollow > span > button");\n' +
            '                        followers.insertRemoveUnfollow(db,"' + id + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("UnFollowed: ' + username + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + username + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '");});\n' +
            '                                    followers.removeUser(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so user removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + username + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + username + '.png")\n' +
            '                                        .then(function(){console.log("' + username + '.png");\n' +
            '                                            followers.sendMail("' + username + '", "user");\n' +
            '                                            this.endAll()\n' +
            '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n' +
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '    })\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '   .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n\n\n'

        fs.appendFileSync("./bot/" + toRun + ".js", data)
    }

    function toLikeAppend(id) {

        var url = id
        var spitted = url.split("/")[4];
        //var data = '\n.url("' + url + '").pause('+randomIntFromInterval(config.pauses-3,config.pauses)+').isExisting("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a").then(function(isThere){    if(isThere){this.click("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a"); followers.updateRemoveLike(db,"' + id + '",function(err){if(err) { console.log(err); throw err; }else{console.log("Liked: ' + id + '")} }) ; }else{ this.isExisting("div.root > div.page > div.main > div.error-container").then(function(isThere){ if(isThere) { console.log("' + id + '"," Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + url + '")}); followers.removeLike(db,"' + id + '", function(err){ if(!err)console.log("so link removed "); }); }else{ console.log("' + spitted + '","Element Not Found"); this.saveScreenshot("./screenshots/' + spitted + '.png").then(function(){console.log("' + spitted + '.png");followers.sendMail("'+spitted+'", "post");  this.endAll().then(function(){ throw { error: "some went wrong!!" }; });;  }); } }) ; }}).pause('+randomIntFromInterval(config.pauses-3,config.pauses)+')\n';
        var data =
            '    .url("' + url + '")\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 1000, config.pauses) + ')\n' +
            '    .isExisting("a[data-reactid=\'.0.2.0.1.$loginLink\']")\n' +
            '    .then(function (login) {\n' +
            '        if(login)\n' +
            '        { \n' +
            '            this.url("https://www.instagram.com/accounts/login")\n' +
            '                .getHTML("form[data-reactid=\'.0.0.1.0.1\']", function (err, res) {\n' +
            '                })\n' +
            '                .setValue("#lfFieldInputUsername", "' + config.instagram.username + '")\n' +
            '                .setValue("#lfFieldInputPassword", "' + config.instagram.password + '")\n' +
            '                .click("button[data-reactid=\'.0.0.1.0.1.3\']")\n' +
            '                .pause(2000)\n' +
            '                .getCookie()\n' +
            '                .then(function(cookies_){\n' +
            '                    console.log(cookies_.length);\n' +
            '                    cookies_.forEach(function(cookie){\n' +
            '                       Cookie.setter(cookie.name, cookie);\n' +
            '                    });\n' +
            '                   console.log("Saved New Cookies to File for next time Use")\n' +
            '                   Cookie.save()\n' +
            '                })\n' +
            '                .pause(500)\n' +
            '                .url("' + url + '")\n' +
            '                .isExisting("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a")\n' +
            '                .then(function(a){\n' +
            '                    if(a)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a");\n' +
            '                        followers.updateRemoveLike(db,"' + id + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("Liked: ' + id + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + id + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + spitted + '.png")\n' +
            '                                        .then(function(){console.log("' + url + '");});\n' +
            '                                    followers.removeLike(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so link removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + spitted + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + spitted + '.png")\n' +
            '                                        .then(function(){console.log("' + spitted + '.png");\n' +
            '                                            followers.sendMail("' + spitted + '", "post");\n' +
            '                                            followers.removeLike(db,"' + id + '",function(err){\n' +
            '                                                   if(!err) console.log(" so link removed"); });\n' +
            // '                                            this.endAll()\n'+
            // '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n'+
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '        else\n' +
            '        {\n' +
            '            this.isExisting("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a")\n' +
            '                .then(function(a){\n' +
            '                    if(a)\n' +
            '                    {\n' +
            '                        this.click("#react-root > section > main > div > div > article > div.-cx-PRIVATE-PostInfo__root.-cx-PRIVATE-Post__mediaInfo > section.-cx-PRIVATE-PostInfo__feedback.-cx-PRIVATE-PostInfo__feedbackSidebarVariant > a");\n' +
            '                        followers.updateRemoveLike(db,"' + id + '",function(err){\n' +
            '                            if(err)\n' +
            '                            {\n' +
            '                                console.log(err);\n' +
            '                                throw err;\n' +
            '                            }\n' +
            '                            else\n' +
            '                            {\n' +
            '                                console.log("Liked: ' + id + '");\n' +
            '                            }\n' +
            '                        });\n' +
            '                    }\n' +
            '                    else\n' +
            '                    {\n' +
            '                        this.isExisting("div.root > div.page > div.main > div.error-container")\n' +
            '                            .then(function(error){\n' +
            '                                if(error)\n' +
            '                                {\n' +
            '                                    console.log("' + id + '"," Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + spitted + '.png")\n' +
            '                                        .then(function(){console.log("' + url + '");});\n' +
            '                                    followers.removeLike(db,"' + id + '",function(err){\n' +
            '                                        if(!err) console.log(" so post removed"); });\n' +
            '                                }\n' +
            '                                else\n' +
            '                                {\n' +
            '                                    console.log("' + spitted + '","Element Not Found");\n' +
            '                                    this.saveScreenshot("./screenshots/' + spitted + '.png")\n' +
            '                                        .then(function(){console.log("' + spitted + '.png");\n' +
            '                                            followers.sendMail("' + spitted + '", "post");\n' +
            '                                            followers.removeLike(db,"' + id + '",function(err){\n' +
            '                                                 if(!err) console.log(" so link removed"); });\n' +
            // '                                            this.endAll()\n'+
            // '                                                .then(function(){ throw { error: "some went wrong!!" }; });\n'+
            '                                        });\n' +
            '                                }\n' +
            '                            })\n' +
            '                    }\n' +
            '                })\n' +
            '        }\n' +
            '    })\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n' +
            '    .pause(' + randomIntFromInterval(config.pauses - 500, config.pauses) + ')\n\n\n'
        fs.appendFileSync("./bot/" + toRun + ".js", data)
    }

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function greatestof3(num1, num2, num3) {

        if (num1 > num2) {
            greater = num1;
        } else {
            greater = num2;
        }

        if (greater > num3) {
            greatest = greater;
        } else {
            greatest = num3;
        }

        return greatest;
    }

}
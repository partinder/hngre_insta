/**
 * Created by Partinder on 9/10/15.
 */
var append = require("./append")
var mongoClient = require("mongodb").MongoClient

mongoClient.connect("mongodb://localhost/db_hngreV2", function(err, db) {
    if (err) throw err
    else {
        append(db, function() {
            console.log("All Appends done")
            console.log("Executing BOT")
            var toRun = require("./toRun")
            toRun(db, function() {
                console.log("Done Follow and Unfollow")

            })
        })
    }
})
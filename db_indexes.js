'use strict';

var async = require("async");

var index = {};

index.ensure = function(db, cb) {
    var our_followers = db.collection("our_followers");
    var our_followings = db.collection("our_followings");
    var users = db.collection("users");
    var get_followers = db.collection("get_followers");
    var posts = db.collection("posts");
    var to_follow = db.collection("to_follow");
    var followed = db.collection("followed");
    var to_unfollow = db.collection("to_unfollow");
    var unfollowed = db.collection("unfollowed");
    var to_like = db.collection("to_like");
    var likes = db.collection("likes");

    async.parallel([

        function(callback) {
            our_followers.ensureIndex({
                id: 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }

            });
        },
        function(callback) {
            our_followings.ensureIndex({
                id: 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }

            });
        },
        function(callback) {
            to_follow.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            followed.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            to_unfollow.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            unfollowed.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            users.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });

        },
        function(callback) {
            get_followers.ensureIndex({
                "user.id": 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            posts.ensureIndex({
                id: 1,
                userId: 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            to_like.ensureIndex({
                id: 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            likes.ensureIndex({
                userId: 1,
                id: 1
            }, {
                unique: true,
                background: true,
                dropDups: true,
                w: 1
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        }
    ], function(err, results) {
        if (err) {
            console.log(err);
            throw err;
        } else {
            //console.log(results);
        }
        cb(err);
    });

};

module.exports = index;
/**
 * Created by Partinder on 9/24/15.
 */
/**
 * Created by Partinder on 9/23/15.
 */


'use strict';

var MongoClient = require("mongodb").MongoClient;
var config = require("./commons/config");
var url = "mongodb://" + config.host + "/db_hngreV2_bot";
var append = require("./bot/appendRe");
var fs = require("fs");
var jsonfile = require("jsonfile")

var Cookie = {
    cookies: {
        ig_pr: {},
        ig_vw: {},
        ds_user_id: {},
        mid: {},
        sessionid: {},
        csrftoken: {}
    },
    getter: function() {
        return jsonfile.readFileSync("./bot/cookie.json");
    },
    setter: function(key, value) {
        this.cookies[key] = value;
    },
    save : function(){
        jsonfile.writeFileSync("./bot/cookie.json",this.cookies)
    }
};

    //var now = new Date()
    //console.log(now.toString())
MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log("Some Err")
            console.log(err);
            throw err;
        } else {
            console.log("Conneced to DB");
            var date1 = new Date();
            console.log("Start at " + date1);
            var date = new Date().getTime();
            var toRun = date.toString();

           fs.writeFileSync("./bot/" + toRun + ".js", "", "utf-8");
            console.log(toRun)

            append(toRun, db, function() {
                console.log("All Appended");
                require("./bot/" + toRun)(db, Cookie, function(that) {
                    var date2 = new Date();
                    console.log("Finished at " + date2);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffMinutes = Math.ceil(timeDiff / (1000 * 60));
                    console.log("Total time taken is %s Minutes", diffMinutes);
                    that.endAll()
                    fs.unlinkSync("./bot/" + toRun + ".js");
                });
            });
        }
    });



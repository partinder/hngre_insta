'use strict';

var async = require("async");
var config = require("./commons/config");
var database = {};

database.create = function(db, cb) {

    async.parallel([

        function(callback) {
            db.createCollection("our_followers", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("our_followings", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("users", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },

        function(callback) {
            db.createCollection("get_followers", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("posts", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("to_follow", {
                //max: config.max_to_follow
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("followed", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("to_unfollow", {
                //max: config.max_to_follow
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("unfollowed", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("to_like", {
                //max: config.max_to_like
            }, function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        },
        function(callback) {
            db.createCollection("likes", function(err, r) {
                if (err) {
                    callback(err, null);

                } else {
                    callback(null, r);
                }
            });
        }
    ], function(err, results) {
        if (err) {
            console.log(err);
            throw err;
        } else {
            //console.log(results);
        }
        cb(err);
    });

};

database.remove = function(db, arr, cb) {
    var toFollow = db.collection("to_follow");
    var toUnfollow = db.collection("to_unfollow");
    var toLike = db.collection("to_like");
    var likes = db.collection("likes");
    var unfollowed = db.collection("unfollowed");
    var followed = db.collection("followed");
    var ourFollowers = db.collection("our_followers");
    var ourFollowings = db.collection("our_followings");
    console.log(arr);

    async.parallel([

        function(callback) {
            if (arr.to_follow) {
                console.log("Start - remove to_follow");
                toFollow.removeMany();
                console.log("Done - remove to_follow");

            }
            callback(null);
        },
        function(callback) {
            if (arr.to_unfollow) {

                console.log("Start - remove to_unfollow");
                toUnfollow.removeMany();
                console.log("Done - remove to_unfollow");

            }
            callback(null);
        },
        function(callback) {
            if (arr.to_like) {
                console.log("Start - remove to like");
                toLike.removeMany();
                console.log("Done - remove to like");
            }
            callback(null);
        },
        function(callback) {
            if (arr.likes) {
                console.log("Start - remove likes");
                likes.removeMany();
                console.log("Done - remove likes");
            }
            callback(null);
        },
        function(callback) {
            if (arr.unfollowed) {
                console.log("Start - remove unfollowed");
                unfollowed.removeMany();
                console.log("Done - remove unfollowed");
            }
            callback(null);
        },
        function(callback) {
            if (arr.followed) {
                console.log("Start - remove followed");
                followed.removeMany();
                console.log("Done - remove followed");
            }
            callback(null);
        },
        function(callback) {
            if (arr.our_followers) {
                console.log("Start - remove our_followers");
                ourFollowers.removeMany();
                console.log("Done - remove our_followers");
            }
            callback(null);
        },
        function(callback) {
            if (arr.ourFollowings) {
                console.log("Start - remove our_followings");
                ourFollowings.removeMany();
                console.log("Done - remove our_followings");
            }
            callback(null);
        }
    ], function(err, result) {
        if (err) {
            console.log(err);
            throw err;
        }
        cb(null, result);
    });

};

module.exports = database;
'use strict';

var instagram = {};

instagram.findUserWhoLikes = function(min, max, db, callback) {
    var MerchantPics = db.collection("merchantPics");

    MerchantPics.aggregate([{
        $project: {
            merchantId: 1,
            likes: 1
        }
    }, {
        $unwind: "$likes.data"
    }, {
        $group: {
            _id: "$likes.data.id",
            merchants: {
                $addToSet: "$merchantId"
            }
        }
    }, {
        $unwind: "$merchants"
    }, {
        $group: {
            _id: "$_id",
            count: {
                $sum: 1
            },
            mcts: {
                $push: "$merchants"
            }
        }
    }, {
        $match: {
            count: {
                $gte: min,
                $lt: max
            }
        }
    }], {
        allowDiskUse: true,
        cursor: {}
    }).toArray(function(err, result) {
        if (!res) {
            if (err) {
                console.log(err);
                callback(err, []);
            } else {
                console.log(result.length);
                callback(null, result);
            }
        } else {
            if (err) {
                console.log(err);
                res.json(err);
            } else {
                console.log(result.length);
                res.json({
                    data: result.slice(0, 2),
                    count: result.length
                });

            }
        }
    });
};

instagram.findUsersWhoCommented = function(min, max, db, callback) {

    var MerchantPics = db.collection("merchantPics");

    MerchantPics.aggregate([{
        $project: {
            merchantId: 1,
            comments: 1
        }
    }, {
        $unwind: "$comments.data"
    }, {
        $group: {
            _id: "$comments.data.from.id",
            merchants: {
                $addToSet: "$merchantId"
            }
        }
    }, {
        $unwind: "$merchants"
    }, {
        $group: {
            _id: "$_id",
            count: {
                $sum: 1
            },
            mcts: {
                $push: "$merchants"
            }
        }
    }, {
        $match: {
            count: {
                $gte: min,
                $lt: max
            }
        }
    }], {
        allowDiskUse: true,
        cursor: {}
    }).toArray(function(err, result) {
        if (!res) {
            if (err) {
                console.log(err);
                callback(err, []);
            } else {
                console.log(result.length);
                callback(null, result);
            }
        } else {
            if (err) {
                console.log(err);
                res.json(err);
            } else {
                console.log(result.length);
                res.json({
                    data: result.slice(0, 2),
                    count: result.length
                });

            }
        }
    });
};

instagram.findUsersWhoPostedOnLocations = function(min, max, db, callback) {

    var MerchantPics = db.collection("merchantPics");

    MerchantPics.aggregate([{
        $project: {
            merchantId: 1,
            user: 1,
            link:1
        }
    }, {
        $group: {
            "_id": "$user.id",
            merchants: {
                $addToSet: "$merchantId"
            },
            links:{
                $addToSet:"$link"
            },
            user:{
                $last:"$user"
            }

        }
    }, {
        $unwind: "$merchants"
    }, {
        $group: {
            _id: "$_id",
            count: {
                $sum: 1
            },
            mcts: {
                $push: "$merchants"
            },
            user:{
                $last:"$user"
            },
            links:{
                $last:"$links"
            }
        }
    }, {
        $match: {
            count: {
                $gte: min,
                $lt: max
            }
        }
    }],{
        allowDiskUse:true
    }).toArray(function(err, result) {
        if (err) {
            console.log(err);
            callback(err, []);
        } else {
            console.log(result.length);
            callback(null, result);
        }
    });
};

module.exports = instagram;
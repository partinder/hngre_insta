'use strict';

var schedule = require("node-schedule");
var index = require("./index");
var MongoClient = require("mongodb").MongoClient;
var config = require("./commons/config");
var url1 = "mongodb://" + config.host + "/db_hngreV2";
var url = "mongodb://" + config.host + "/db_hngreV2_bot";
var append = require("./bot/append");
var fs = require("fs");
var jsonfile = require("jsonfile")

var Cookie = {
    cookies: {
        ig_pr: {},
        ig_vw: {},
        ds_user_id: {},
        mid: {},
        sessionid: {},
        csrftoken: {}
    },
    getter: function() {
        return jsonfile.readFileSync("./bot/cookie.json");
    },
    setter: function(key, value) {
        this.cookies[key] = value;
    },
    save : function(){
        jsonfile.writeFileSync("./bot/cookie.json",this.cookies)
    }
};


var j = schedule.scheduleJob({
    hour: 8,
    minute: 0
}, function() {

    MongoClient.connect(url1, function(err, db1) {
        if (err) {
            console.log(err);
            throw err;
        }
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                console.log("Conneced to DB");
                var date1 = new Date();
                console.log("Start at " + date1);
                index.init(db, db1, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        db1.close();
                        var date = new Date().getTime();
                        var toRun = date.toString();
                        var toLike1 = date + "1";
                        var toLike2 = date + "2";

                        fs.writeFileSync("./bot/" + toRun + ".js", "", "utf-8");
                        fs.writeFileSync("./bot/" + toLike1 + ".js", "", "utf-8");
                        fs.writeFileSync("./bot/" + toLike2 + ".js", "", "utf-8");

                        append(toRun, toLike1, toLike2, db, function() {
                            console.log("All Appended");
                            require("./bot/" + toRun)(db, Cookie, function(that) {
                                require("./bot/" + toLike1)(db, that, function() {
                                    require("./bot/" + toLike2)(db, that, function() {
                                        var date2 = new Date();
                                        console.log("Finished at " + date2);
                                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                        var diffMinutes = Math.ceil(timeDiff / (1000 * 60));
                                        console.log("Total time taken is %s Minutes", diffMinutes);
                                        that.endAll()
                                        fs.unlinkSync("./bot/" + toRun + ".js");
                                        fs.unlinkSync("./bot/" + toLike1 + ".js");
                                        fs.unlinkSync("./bot/" + toLike2 + ".js");

                                    });
                                });
                            });
                        });
                    }
                });
            }

        });
    });
});

module.exports = j;
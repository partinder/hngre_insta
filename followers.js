'use strict';

var instagram = require("./instagram");
var request = require("request");
var config = require("./commons/config");
var nodemailer = require("nodemailer");
//var ISODate = require("mongodb").I

var accessToken = config.accessToken;

var userId = config.userId;

var followers = {};

followers.store = function(db, callback) {

    function getFollowers(cursor) {
        var api = "https://api.instagram.com/v1/users/" + userId + "/followed-by?access_token=" + accessToken + "&cursor=" + cursor;
        //console.log(api);
        request.get(api, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                body = JSON.parse(body);
                console.log("My Followers Page#=> ", userId, cursor, body.data.length);
                if (body.data) {
                    //console.log(body.pagination);
                    var collection = db.collection("our_followers");
                    collection.insert(body.data, function(err, r) {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                        //console.log(r.result);
                        if (body.pagination && body.pagination.next_cursor) {
                            setTimeout(function() {
                                getFollowers(body.pagination.next_cursor);
                            }, config.time_out);
                        } else {
                            getFollowings("");
                        }
                    });

                }

            }
        });
    }

    function getFollowings(cursor) {
        var api = "https://api.instagram.com/v1/users/" + userId + "/follows?access_token=" + accessToken + "&cursor=" + cursor;
        //console.log(api);
        request.get(api, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                body = JSON.parse(body);
                console.log("My Followings Page#=> ", userId, cursor, body.data.length);
                if (body.data) {
                    //console.log(body.pagination);
                    var collection = db.collection("our_followings");
                    collection.insert(body.data, function(err, r) {
                        if (err) {
                            console.log(err);
                            throw err;
                        }
                        //console.log(r.result);
                        if (body.pagination && body.pagination.next_cursor) {
                            setTimeout(function() {
                                getFollowings(body.pagination.next_cursor);
                            }, config.time_out);
                        } else {
                            if (typeof callback === "function") {
                                callback(null, {
                                    done: true
                                });
                            }
                        }
                    });

                }

            }
        });
    }
    getFollowers("");

};

followers.filterWithOurFollowings = function(db, callback) {
    var our_followings = db.collection("our_followings");
    var followed = db.collection("followed");
    var users = db.collection("users");
    var date = new Date();

    our_followings.find().toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }

        var total = data.length;

        function update() {
            var id = data[total - 1].id;
            var username = data[total - 1].username;
            users.updateOne({
                _id: id
            }, {
                $set: {
                    deleted: true,
                    followedAt: date
                }
            }, function(r) {
                if (r) {
                    //console.log(r.result);
                }

                if (--total) {
                    update();
                } else {
                    if (typeof callback === "function") {
                        callback(null, {
                            done: true
                        });
                    }
                }
            });
        }

        if (total) {
            update();
        }

    });
};

followers.filterWithOurFollowers = function(db, callback) {
    var our_followers = db.collection("our_followers");
    var users = db.collection("users");
    var getfollowers = db.collection("get_followers");

    our_followers.find().toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        var total = data.length;

        function update() {
            var id = data[total - 1].id;
            //console.log(total, id);
            users.findOne({
                _id: id
            }, function(err, doc) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                //console.log(doc);
                if (doc) {

                    getfollowers.update({
                        _id: id
                    }, doc, {
                        upsert: true
                    }, function(err, r) {
                        if (err) {
                            console.log(err);
                            //throw err;
                        }
                        //console.log(r.result);

                        if (--total) {
                            update();
                        } else {
                            // console.log("ALL DONE");
                            if (typeof callback === "function") {
                                callback(null, {
                                    done: true
                                });
                            }
                        }
                    });

                } else {
                    if (--total) {
                        update();
                    } else {
                        //console.log("ALL DONE");
                        if (typeof callback === "function") {
                            callback(null, {
                                done: true
                            });
                        }
                    }
                }
            });
        }
        if (total) {
            update();
        }
    });
};

followers.tobefollowed = function(db, callback) {
    var users = db.collection("users");
    var to_follow = db.collection("to_follow");
    var total = 0;
    users.find({
        deleted: {
            "$ne": true
        }
    }, {
        limit: config.max_to_follow
    }).toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }

        function insert() {
            console.log(total);
            var d = data[total - 1];
            to_follow.updateOne({
                _id: d._id
            }, d, {
                upsert: true
            }, function(r) {
                if (r) {
                    //console.log(r.result);
                }
                if (--total) {
                    insert();
                } else {
                    if (typeof callback === "function") {
                        callback(null, data);
                    }
                }

            });
        }

        if (data) {
            total = data.length;
            if (total) {
                insert();
            } else {
                if (typeof callback === "function") {
                    callback('no data', null);
                }
            }

        } else {
            if (typeof callback === "function") {
                callback('no data', null);
            }
        }
    });
};

followers.tobeunfollowed = function(db, callback) {
    var our_followers = db.collection("our_followers");
    var followed = db.collection("followed");
    var getfollowers = db.collection("get_followers");
    var tounfollow = db.collection("to_unfollow");

    var dateTo = new Date();
    var dateFrom = new Date();

    if (config.no_of_days) {
        dateFrom.setDate(dateFrom.getDate() - config.no_of_days);
        dateFrom.setHours(0);
        dateFrom.setMinutes(0);
        dateFrom.setSeconds(0);
        dateFrom.setMilliseconds(0);

        dateTo.setDate(dateFrom.getDate() + config.days);
        dateTo.setHours(dateFrom.getHours() - 0);
        dateTo.setMinutes(dateFrom.getMinutes() - 0);
        dateTo.setSeconds(dateFrom.getSeconds() - 0);
        dateTo.setMilliseconds(dateFrom.getMilliseconds() - 0);
    } else {

        if (config.no_of_hours) {
            dateFrom.setMinutes(0);
            dateFrom.setSeconds(0);
            dateFrom.setMilliseconds(0);

            dateTo.setHours(dateFrom.getHours() - config.no_of_hours);
            dateTo.setMinutes(dateFrom.getMinutes() - 0);
            dateTo.setSeconds(dateFrom.getSeconds() - 0);
            dateTo.setMilliseconds(dateFrom.getMilliseconds() - 0);
        } else {
            if (config.no_of_minutes) {
                if (config.no_of_minutes <= 15) {
                    if (dateFrom.getMinutes() <= 15) {
                        dateFrom.setMinutes(0);
                    } else if (dateFrom.getMinutes() > 15 && dateFrom.getMinutes() <= 30) {
                        dateFrom.setMinutes(15);
                    } else if (dateFrom.getMinutes() > 30 && dateFrom.getMinutes() <= 45) {
                        dateFrom.setMinutes(30);
                    } else {
                        dateFrom.setMinutes(45);
                    }
                    dateFrom.setSeconds(0);
                    dateFrom.setMilliseconds(0);

                    dateTo.setMinutes(dateFrom.getMinutes() - config.no_of_minutes);
                    dateTo.setSeconds(dateFrom.getSeconds() - 0);
                    dateTo.setMilliseconds(dateFrom.getMilliseconds() - 0);
                } else {
                    if (dateFrom.getMinutes() < 30) {
                        dateFrom.setMinutes(0);
                    } else {
                        dateFrom.setMinutes(30);
                    }
                    dateFrom.setSeconds(0);
                    dateFrom.setMilliseconds(0);

                    dateTo.setMinutes(dateFrom.getMinutes() - config.no_of_minutes);
                    dateTo.setSeconds(dateFrom.getSeconds() - 0);
                    dateTo.setMilliseconds(dateFrom.getMilliseconds() - 0);
                }

            }
        }

    }


    var total = 0;

    console.log(dateTo);
    console.log(dateFrom);

    followed.find({
        followedAt: {
            //$gte: //new Date(dateTo),
            //$lt: new Date(dateFrom)
            $gte: new Date(dateFrom.toISOString()),
            $lt : new Date(dateTo.toISOString())
        }

    }, {
        limit: config.max_to_unfollow
    }).toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        console.log(data);

        function update() {
            var followedData = data[total - 1];
            var id = followedData.user.id;

            console.log(total);
            tounfollow.updateOne({
                "user.id": id
            }, followedData, {
                upsert: true
            }, function(r) {
                if (r) {
                    //console.log(r.result);
                }
                followed.removeOne({
                    "user.id": id
                }).then(function(err, r) {
                    if (err) {
                        //console.log(err);
                        //throw err;
                    }
                    //console.log(r);
                    if (--total) {
                        update();
                    } else {
                        console.log("ALL DONE");
                        if (typeof callback === "function") {
                            callback(null, {
                                done: true
                            });
                        }
                    }
                });
            });
        }
        if (data) {
            total = data.length;
            if (total) {
                update();
            } else {
                callback(null, {
                    done: true
                });
            }
        } else {
            callback(null, {
                done: true
            });
        }

    });
};

followers.tobeliked = function(db, callback) {
    var likes = db.collection("likes");
    var to_like = db.collection("to_like");
    var limit = config.max_to_like;
    likes.count(function(err, count) {
        if (err) {
            console.log(err);
            throw err;
        }
        console.log(count);
        function insert() {

            var rand = Math.floor(Math.random() * (count + 1 - 1) + 1);
            likes.find({
                liked: {
                    "$ne": true
                }
            }, {
                limit: 1,
                skip: rand
            }).toArray(function(err, docs) {
                if (err) {
                    console.log(err);
                    throw err;
                }

                if (docs) {
                    //console.log(docs);
                    if (docs && docs.length) {
                        docs = docs[0];
                    }
                    to_like.updateOne({
                        id: docs.id
                    }, docs, {
                        upsert: true
                    }, function(r) {
                        if (r) {
                            //console.log(r.result);
                        }
                        to_like.count(function(err, total) {
                            if (err) {
                                console.log(err);
                                throw err;
                            }
                            //console.log(total);
                            if (total < limit) {
                                insert();
                            } else {
                                callback(null, {
                                    done: true
                                });
                            }
                        });
                    });
                } else {
                    callback(null, {
                        done: false
                    });
                }

            });
        }

        if (count) {
            if (count < limit) {
                limit = count;
                insert();
            } else {
                insert();
            }
        } else {
            callback(null, {
                done: true
            });
        }

    });
};

followers.likes = function(db, callback) {
    var posts = db.collection("posts");
    var tofollow = db.collection("to_follow");
    var likes = db.collection("likes");

    tofollow.find().toArray(function(err, data) {
        if (err) {
            console.log(err);
            throw err;
        }
        var arr = [];
        if (data) {
            data.forEach(function(d) {
                arr.push(d.user.id);
            });
            posts.find({
                userId: {
                    "$in": arr
                }
            }).toArray(function(err, p) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                if (p) {
                    console.log(p.length);
                    likes.insert(p, function(err, r) {
                        if (err) {
                            console.log(err);
                            //throw err;
                        }
                        callback(null, {
                            done: true
                        });
                    });
                } else {
                    callback(null, {
                        done: true
                    });
                }

            });
        } else {
            callback(null, {
                done: true
            });
        }

    });

};

followers.insertRemoveUnfollow = function(db, id, cb) {
    var toUnfollow = db.collection("to_unfollow");
    var unfollowed = db.collection("unfollowed");

    console.log(id);
    toUnfollow.findOne({
        "user.id": id
    }, function(err, doc) {
        if (err) {
            //  console.log(err);
            //throw err;
        }
        if (doc) {
            unfollowed.updateOne({
                    "user.id": id
                }, doc, {
                    upsert: true
                },
                function(r) {
                    if (r) {
                        //console.log(r.result);
                    }
                    toUnfollow.removeOne({
                        "user.id": id
                    }).then(function(err, r) {
                        if (err) {
                            //console.log(err);
                            //throw err;
                        }
                        //console.log(r);
                        cb(null);
                    });
                });
        } else {
            cb(null);
        }

    });

};

followers.insertRemoveFollow = function(db, id, username, cb) {
    var toFollow = db.collection("to_follow");
    var followed = db.collection("followed");
    var users = db.collection("users");
    var date = new Date();

    var data = {
        user: {
            id: id,
            username: username
        },
        followedAt: date
    };

    //console.log("I Followed ", id);
    users.updateOne({
        _id: id
    }, {
        $set: {
            deleted: true,
            followedAt: date
        }
    }, function(r) {
        if (r) {
            //console.log(r.result);
        }
        followed.updateOne({
                "user.id": id
            }, data, {
                upsert: true
            },
            function(r) {
                if (r) {
                    //console.log(r.result);
                }
                toFollow.removeOne({
                    "user.id": id
                }).then(function(err, r) {
                    if (err) {
                        //console.log(err);
                        //throw err;
                    }
                    //console.log(r);
                    cb(null);
                });
            });

    });
};

followers.updateRemoveLike = function(db, id, cb) {
    var likes = db.collection("likes");
    var toLike = db.collection("to_like");
    var date = new Date();
    var data = {
        liked: true,
        likedAt: date
    };

    likes.updateOne({
        id: id
    }, {
        $set: data
    }, function(r) {
        if (r) {
            //console.log(r.result);
        }
        toLike.removeOne({
            id: id
        }).then(function(err, r) {
            if (err) {
                //console.log(err);
                //throw err;
            }
            //console.log(r);
            cb(null);
        });
    });
};

followers.removeUser = function(db, id, cb) {
    var users = db.collection("users");
    var to_follow = db.collection("to_follow");
    users.removeOne({
        "user.id": id
    }, function(err, r) {
        if (err) {
            //console.log(err);
        }
        to_follow.removeOne({
            "user.id": id
        }, function(err, r) {
            if (err) {
                //console.log(err);
            }
            cb(null);
        });

    });
};

followers.removeLike = function(db, id, cb) {
    var likes = db.collection("likes");
    var to_like = db.collection("to_like");
    likes.removeOne({
        id: id
    }, function(err, r) {
        if (err) {
            //console.log(err);
        }
        to_like.removeOne({
            id: id
        }, function(err, r) {
            if (err) {
                //console.log(err);
            }
            cb(null);
        });

    });
};

followers.sendMail = function(username, type) {
    console.log("sendMail",username,type);
    var receivers = config.receiverAddresses;
    var transporter = nodemailer.createTransport(config.gmail);
    var mailOptions = {
        from: 'Hngre Bot ✔ <' + config.senderAddress + '>', // sender address 
        to: receivers.join(", "), // list of receivers 
        subject: 'Bot ' + type + ' Error : ' + username, // Subject line 
        text: 'Bot ' + type + ' Error : ' + username, // plaintext body 
        html: '<b>Bot ' + type + ' Error :' + username + ' </b><br>Screenshot : <img src="cid:unique@kreata.ee"/>',
        attachments: [{
            filename: username + '.png',
            path: './screenshots/' + username + '.png',
            cid: 'unique@kreata.ee' //same cid value as in the html img src 
        }]
    };
    console.log(mailOptions);
    // send mail with defined transport object 
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
            //return console.log(error);
        }
        console.log('Message sent: ' + info.response);

    });

};

module.exports = followers;
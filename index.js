'use strict';

var collections = require("./collections");
var dbIndexes = require("./db_indexes");
var followers = require("./followers");
var aggregate = require("./aggregate");
var config = require("./commons/config");
var async = require("async");

var index = {};

index.init = function(db, db1, cb) {
    //console.log(config);

    function initial(callback) {
        console.log("Start Aggregation");
        aggregate.init(db, db1, function(err) {
            console.log("Done Aggregation");
            callback(null);
        });
    }

    function collectionsIndexes(callback) {
        console.log("Start - Creating Collections");
        collections.create(db, function(err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done - Creating Collection");
            }
            console.log("Start - Indexing");
            dbIndexes.ensure(db, function(err, r) {
                if (err) {
                    console.log(err);
                    throw err;
                } else {
                    console.log("Done - Indexing");
                }
                callback(null);

            });
        });
    }

    function storeFollowers(callback) {
        console.log("Start - To store followers");
        followers.store(db, function(err, r) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log("Done - To store followers");
            callback(null);
        });
    }

    function filterFollowers(callback) {
        console.log("Start - match followers");
        followers.filterWithOurFollowers(db, function(err, r) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log("Done - match followers");
            callback(null);
        });
    }

    function filterFollowings(callback) {
        console.log("Start - match followings");
        followers.filterWithOurFollowings(db, function(err, r) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log("Done - match followings");
            callback(null);
        });
    }


    function toBeFollowed(callback) {
        console.log("Start - To follow");
        followers.tobefollowed(db, function(err, r) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done - To follow");
            }
            callback(null);
        });
    }

    function toBeUnfollowed(callback) {
        console.log("Start - To unfollow");
        followers.tobeunfollowed(db, function(err, r) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done - To unfollow");
            }
            callback(null);
        });
    }

    function toBeLike(callback) {
        console.log("Start - To like");
        followers.tobeliked(db, function(err, r) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done - To like");
            }
            callback(null);
        });
    }

    function likes(callback) {
        console.log("Start - Likes");
        followers.likes(db, function(err, r) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done - Likes");
            }
            callback(null);
        });
    }

    function clearRecords(callback) {
        console.log("Start - Clear Records");

        collections.remove(db, {
            to_follow: true,
            to_unfollow: true,
            to_like: true,
            likes: false,
            unfollowed: false,
            followed: false,
            our_followers: true,
            ourFollowings: true
        }, function(err, r) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log("Done - Clear Records");
            callback(null);
        });

    }

    function init(funcArr) {
        async.waterfall(funcArr, function(err, result) {
            if (err) {
                console.log(err);
                throw err;
            }
            console.log("Complete");
            cb(null);
        });
    }

    /*
    param 1 : create collection and index it.
    param 2 : get our followers through instagram api and store it into our_followers collection
    param 3 : compare users with our_followers collection and remove matched our follower from users and store into get_followers.
    param 4 : get 240 user from users collection and flag them as deleted
    param 5 : get all posts of 240 users as to be followed and store into likes collection
    param 6 : get 900 random post to be liked from likes collection and store into to_like collection
    param 7 : get users to be unfollowd and store into to_unfollow collection
    param 8 : to remove all documents of to_follow, to_unfollow, likes, to_like collections
*/
    init([
        //initial,
        collectionsIndexes,
        clearRecords,
        storeFollowers,
        filterFollowers,
        filterFollowings,
        toBeFollowed,
        likes,
        toBeLike,
        toBeUnfollowed
    ]);

};

module.exports = index;
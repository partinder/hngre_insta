/**
 * Created by Partinder on 9/23/15.
 */

'use strict';
var schedule = require("node-schedule");
var index = require("./index");
var MongoClient = require("mongodb").MongoClient;
var config = require("./commons/config");
var url1 = "mongodb://" + config.host + "/db_hngreV2";
var url = "mongodb://" + config.host + "/db_hngreV2_bot";
var append = require("./bot/appendRe");
var fs = require("fs");
var jsonfile = require("jsonfile")

var Cookie = {
    cookies: {
        ig_pr: {},
        ig_vw: {},
        ds_user_id: {},
        mid: {},
        sessionid: {},
        csrftoken: {}
    },
    getter: function() {
        return jsonfile.readFileSync("./bot/cookie.json");
    },
    setter: function(key, value) {
        this.cookies[key] = value;
    },
    save : function(){
        jsonfile.writeFileSync("./bot/cookie.json",this.cookies)
    }
};

//var j = schedule.scheduleJob("45 7 * * *", function() {
var j = schedule.scheduleJob("0 7-12,14-19 * * *", function() { // 9 Hours
//var j = schedule.scheduleJob("*/2 * * * *", function() {

    var now = new Date()
    console.log(now.toString())
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log(err);
            throw err;
        } else {
            console.log("Conneced to DB");
            var date1 = new Date();
            console.log("Start at " + date1);
            var date = new Date().getTime();
            var toRun = date.toString();

            fs.writeFileSync("./bot/" + toRun + ".js", "", "utf-8");

            append(toRun, db, function() {
                console.log("All Appended");
                require("./bot/" + toRun)(db, Cookie, function(that) {
                    var date2 = new Date();
                    console.log("Finished at " + date2);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffMinutes = Math.ceil(timeDiff / (1000 * 60));
                    console.log("Total time taken for Running Bot %s Minutes", diffMinutes);
                    that.endAll()
                    fs.unlinkSync("./bot/" + toRun + ".js");
                });
            });
        }
    });
});

var k =  schedule.scheduleJob("50 6 * * *", function() {
    var date1 = new Date();
    console.log("Start at " + date1);

    MongoClient.connect(url1, function (err, db1) {
        if (err) {
            console.log(err);
            throw err;
        }
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                console.log("Conneced to DB");
                var date1 = new Date();
                console.log("Start at " + date1);
                index.init(db, db1, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        var date2 = new Date();
                        console.log("Finished at " + date2);
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        var diffMinutes = Math.ceil(timeDiff / (1000 * 60));
                        console.log("Total time taken for Setting up collections is %s Minutes", diffMinutes);
                        console.log("Collections Set")
                        //callback()
                    }
                });
            }

        });
    });
})


module.exports.j = j;
module.exports.k = k;


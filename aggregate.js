'use strict';

var instagram = require("./instagram");
var config = require("./commons/config");
var MIN = config.min_merchants;
var MAX = config.max_merchants;

var aggregate = {};

aggregate.init = function(db, db1, cb) {
    instagram.findUsersWhoPostedOnLocations(MIN, MAX, db1, function(err, result) {
        var total = result.length;
        var user = db.collection("users");
        var posts = db.collection("posts");

        function getUsers() {
            console.log(total);
            var d = result[total - 1];
            user.insert(d, function(err, res) {
                if (err) console.log(err);
                else {
                    console.log(res.result);
                    var postArr = [];
                    d.links.forEach(function(link) {
                        postArr.push({
                            "id": link,
                            userId: d.user.id
                        });
                    });
                    posts.insert(postArr, function(err, res) {

                        if (err) console.log(err);
                        else {
                            console.log(res.result);
                            if (--total) {
                                getUsers();
                            } else {
                                console.log("All DONE");    
                                cb(null);
                            }
                        }
                    });
                }
            });
        }
        if (total) {
            getUsers();
        }
    });
};

module.exports = aggregate;
